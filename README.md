# Introduction

Here you can find some Dockerfiles to generate build environments for Espressif WiFi enabled MCUs.

# Usage

You can build the images yourself from the available Dockerfiles, or browse the [already compiled Docker images here](https://gitlab.com/doragasu/esp-dockerfiles/container_registry), and run any of them. For example:

```bash
$ docker run --rm -it registry.gitlab.com/doragasu/esp-dockerfiles/esp-idf:v4.2
```

The first time you run a image it will be pulled from the registry. Once pulled it will be run immediately.

Images in this repository create user `esp` and install the toolchain in `/home/esp/dev` directory. When you run the images, it is recommended to mount the work directory from the host machine, and also to map the serial port corresponding to the device you want to use. For example, a more complete `docker run` invocation can be as follows:

```bash
$ docker run --rm --device=/dev/ttyUSB0 -v $HOME/projects:/home/esp/projects -it registry.gitlab.com/doragasu/esp-dockerfiles/esp-idf:v4.2
```

This is a long command, so it is recommended to create a script to start the image. I usually use this one:

```bash
#!/usr/bin/env bash

DEVEL_BASE=$HOME/src
TARGET_BASE=/home/esp
DEVICES=/dev/ttyUSB0
IMAGE="registry.gitlab.com/doragasu/esp-dockerfiles/esp-idf:v4.2"

abort()
{
	echo "Usage: idf_start [options]"
	echo "Supported options are:"
	echo "    -i image: use specified docker image instead of default"
	echo "    -d: list of devices to map (default: /dev/ttyUSB0)"
	echo "    -h: Print this help screen and exit"

	exit 1
}

while getopts "hi:d:" arg
do
	case $arg in
		i) IMAGE=$OPTARG ;;
		d) DEVICES=$OPTARG ;;
		h) ;&
		?) abort ;;
	esac
done

for DEVICE in $DEVICES
do
	DEVICE_MAP="$DEVICE_MAP --device=$DEVICE"
done

echo Entering container $IMAGE...
docker run --rm $DEVICE_MAP -v $DEVEL_BASE:$TARGET_BASE/src -v ~/.ssh:$TARGET_BASE/.ssh -it $IMAGE
```
Once you are inside the Docker image, just use the build environment as instructed in the SDK documentation.
